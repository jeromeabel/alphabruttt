# Idées de réalisations


V3GA - gravity

Morellet : http://www.lyceelecorbusier.eu/p5js/?p=3016

Sprites jeu.



Exo : blob > frameCount
Formes avec pleins de cercles (bestioles) > couleur change dès qu'elle touche le bord

variable : if

Exo : random walk

Exo : typewriter de formes : frameCount, modulo %, print

Exo : clavier -> forme au hasard (évanouis avec frameCount)




tableau

pop
push
rotate

images

http://www.lyceelecorbusier.eu/p5/hacknimaux/

Sandinson
Charles Csuri Random War, 1967
Manfred Mohr (random walk, cubic limit), Lewitt
jeu de la vie
Morellet
image > ascii

Bob l'éponge
Bestiole
Pong


couleur cercle+background



## AlphaCode
- À chaque caractère on associe une forme géométrique simple.
- typewriter de formes. 


Sandinson :
- touche ENTER : lettres qui bougent. fade, translate, rotate. Voir Sandinson.




- L'inverse, à chaque couleur de pixel (images d'une webcam par exemple), on associe un caractère (classés selon le niveau de blanc). AsciiArt
- À chaque son, on associe une lettre
- A chaque lettre, on associe un son


## AlphaBoulle ;-)
Détournement d'un clavier pour en faire une boulle de touche.
Extension de cette version possible en version bluetooth et code Arduino.

## AlphaBrut
Une caméra prend des photos de dessins/photos/objets qui représentent des lettres. On forme un alphabet qu'on utilise pour afficher des textes de l'atelier Haiku (divagations poétiques) par exemple ou en mode interactif, avec la boule.

## AlphaTrans
On fait passer des lettres d'un ordinateur à un autre (flux)
 
## Désordre alphabétique
Expérience d'écritures : créer un programme qui cache des lettres, inverse des mots, change les lettres, classe les mots selon leur occurences, etc. (liens avec visualisation de données)

Introduction au formulaire Web ?

## AlphaBot
Expériences de dessins par des machines : glyphe, mini drawbot, matrices lumineuses
Bot informatique

## AlphaWars
Texte StarWars