# AlphaBruttt


## Contenu
AlphaBruttt est un parcours d'ateliers destiné à la découverte de la programmation informatique et des arts numériques. Nous réaliserons de petites expérimentations avec Processing ou P5.js, un langage conçu pour faciliter l'apprentissage de la programmation.

Le fil conducteur sera le détournement et la création d'alphabets multimédia, en associant des lettres à des images, à des formes géométriques ou à des sons. L'approche est volontairement pratique et ludique pour rendre tangible certaines notions techniques que pourrions aborder comme les variables, les fonctions, les objets, la couleur, etc. Nous découvrirons aussi quelques oeuvres artistiques pour donner une vision un peu plus générale de cette culture.

## Infos
- Âge : à partir de 15-16 ans, selon la motivation.
- Nombre de participants : entre 8 et 10 personnes
- Intervenant : Jérôme Abel, artiste et formateur multimédia : http://jeromeabel.net
- Niveau requis : avoir déjà utilisé un ordinateur
- Matériel à fournir : connexion Internet, 1 ordinateur pour deux personnes, un vidéo projecteur, salle que l'on peut assombrir pour la vidéo-projection, un système de diffusion sonore et quelques rallonges électriques et multiprises si besoin. Les participants peuvent amener leur ordinateur, feuilles de dessins.
- Logiciels : téléchargement de p5js, installation de Chrome. Possibilité d'utiliser l'éditeur en ligne https://editor.p5js.org

## Évolution de l'apprentissage
- structure du programme
- formes graphiques simples (primitives) et complexes (shape), typographie
- transformations (translate, rotate, scale) + pop/push, couleurs
- interactions
- variables (declare, initialize, use), répétitions, tableaux, fonctions, objets, tests, événements
- structure de contrôle ( conditions, switch)
- images (svg, png, jpg, pdf + export), vidéos (usb, fichier, streaming), sons
- éléments HTML
- 3D
- Shader
- communications OSC, arduino


## Déroulé

**Session 1**:
- Panorama artistique
- Panorama des outils de programmation pour un usage artistique
- Ressources p5js :
	- [Intro to JS: Drawing & Animation](https://www.khanacademy.org/computing/computer-programming/programming)
	- [p5js.org](https://p5js.org) : learn, reference, ...
	- [Vidéos Coding train](https://thecodingtrain.com/Tutorials/) 
	- https://www.lyceelecorbusier.eu/p5js/
	- [Generative Design](http://www.generative-gestaltung.de/2/)
	- [Design génératif - Olivier Evrard] (http://www.olivierevrard.be/master/design_generatif.html)
	- [Design interactif — ECV Bordeaux — 2016](https://github.com/v3ga/Cours_ECV_Bordeaux_2016)
	- [Art, design & algorithme ](http://algorithme.beautifulseams.com/)
	- [Generative design et creative coding](https://www.slideshare.net/arivaux/01-gx-d-generative-design-et-creative-coding)
	- [Introduction à p5js - Bérenger Recoules](https://b2renger.github.io/Introduction_p5js/)
- Création d'une page html, css, js et compréhension de l'articulation entre ces langages.
- Premiers tests avec p5js : structure, couleur, forme, animation, random, interactions avec la souris



**Session 2**
- mouvement avec frameCount + modulo
- création d'un nuage avec une fonction et plusieurs cercles
- déplacements de plusieurs nuages avec une boucle for (+ soleil et texte) et utilisation de pop/push pour isoler les coordonnées des nuages

**Session 3**
- approfondissement des variables : incrémentation, couleur
- condition if et variables pour réaliser la balle qui rebondit

**Session 4**
- création du début du jeu pong : utilisation d'objets, interaction avec le clavier
- démo processing/arduino


## P5JS utilisation
- En local : Atom + Chrome ou FireFox
- En local : Brackets (pas besoin de lancer Chrome) ~serveur
- En ligne : éditeur https://editor.p5js.org (créer un compte)

## Processing.org
Programme à télécharger.
